<?php
use yii\helpers\Html;


/* @var $this yii\web\View */

$this->title = 'Booklan';
?>
<div class="site-index">

    <div class="jumbotron">
        <br><?= Html::a("Autores", ["autores/listar"], ["class" => "btn btn-default"]) ?></br>
        <br><?= Html::a("Libros", ["libros/listar"], ["class" => "btn btn-default"]) ?></br>      
    </div>
   
</div>
