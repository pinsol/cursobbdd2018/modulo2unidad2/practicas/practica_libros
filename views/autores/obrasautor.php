<?php

use yii\grid\GridView;
use yii\helpers\Html;

?>

<h1><?php
//$modelo=$dataProvider->query->one();
//echo $modelo->autor0->nombre;
$nombre
?> </h1>

<?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'nombre',
        'editorial',
        'autor0.nombre',
        [
            'label'=>'portada',
            'format'=>'raw',
            'value' => function($data){
                $url = Yii::getAlias("@web") . "/imgs/libros/" . $data->portada;
                return Html::img($url,[
                    'width'=>"150",
                    'alt'=>'yii']); 
            }
        ],
    ],
]);
?>

<div class="row">
    <div class="col-sm-12">
 <?= \yii\helpers\Html::a( 'Atrás', Yii::$app->request->referrer,[
     'class'=>'btn btn-default'
 ]);?>
    </div>
</div>
