<?php
    use yii\helpers\Html;
?>

<div>
    <h1>
    <?= $model->nombre ?>
    </h1>
</div>
<div class="alineacion-derecha">
    <?= Html::img('@web/imgs/autores/' . $model->foto, [
        'alt' => 'My logo',
        'class' => 'img-responsive ancho2 centrar'
        ]) ?> 
</div>

<div> 
    <?= Html::a("Obras de este autor", ["autores/obras",'id'=>$model->id], ["class" => "btn btn-default"])  ?>
</div>

