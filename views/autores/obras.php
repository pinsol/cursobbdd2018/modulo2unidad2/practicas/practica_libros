<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Autores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autores-inicio">

    <h1><?= Html::encode($this->title) ?></h1>

 
    
    <?php
//        echo GridView::widget([
//        'dataProvider' => $dataProvider,
//        'columns' => [
//            [
//            'label'=>'foto',
//            'format'=>'raw',
//            'value' => function($data){
//                $url = Yii::getAlias("@web") . "/imgs/autores/" . $data->foto;
//                return Html::img($url,[
//                    'width'=>"150",
//                    'alt'=>'yii']); 
//            }
//        ],
//        ],
//    ]); 
    
    
    echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_inicio',
    ]);
 
?>
</div>


