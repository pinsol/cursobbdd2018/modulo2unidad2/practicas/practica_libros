<?php

use yii\db\Migration;

/**
 * Class m190402_111311_autores
 */
class m190402_111311_autores extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("autores", [
           'id'=> $this->primaryKey(),
            'nombre'=>$this->string(50),
            'foto'=>$this->string(30)
        ]);
        
        $this->insert("autores", [
            'nombre'=>'Miguel de Cervantes',
            'foto'=>"foto1.jpg",
        ]);
        
        $this->insert("autores", [       
            'nombre'=>'Aldous L. Huxley',
            'foto'=>'foto2.jpg'
            
        ]);
        
        $this->insert("autores", [
            'nombre'=>'Gabriel García Márquez',
            'foto'=>'foto3.jpg',
        ]);
        
        $this->insert("autores", [
            'nombre'=>'George Orwell',
            'foto'=>"foto4.jpg",
        ]);
        
        $this->insert("autores", [
            'nombre'=>'Ken Follet',
            'foto'=>"foto5.jpg",
        ]);
        
        $this->insert("autores", [
            'nombre'=>'Jane Austen',
            'foto'=>"foto6.jpg",
        ]);
        
        $this->insert("autores", [
            'nombre'=>'Oscar Wilde',
            'foto'=>"foto7.jpg",
        ]);
        
        $this->insert("autores", [
            'nombre'=>'J. R. R. Tolkien',
            'foto'=>"foto8.jpg",
        ]);
        
        $this->insert("autores", [
            'nombre'=>'William Shakespeare',
            'foto'=>"foto9.jpg",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('autores');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190402_111311_autores cannot be reverted.\n";

        return false;
    }
    */
}
