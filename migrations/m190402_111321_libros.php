<?php

use yii\db\Migration;

/**
 * Class m190402_111321_libros
 */
class m190402_111321_libros extends Migration
{
    /**
     * {@inheritdoc}
     */
        
    public function safeUp()
    {
        $this->createTable("libros", [
           'id'=> $this->primaryKey(),
            'nombre'=>$this->string(60),
            'editorial'=>$this->string(50),
            'autor'=>$this->integer(),
            'portada'=>$this->string(30)
        ]);
        
        $this->addForeignKey('fkLibrosAutores',
                'Libros', 'Autor',
                'Autores', 'id',
                'cascade','cascade'
                );
                
        $this->insert("libros", [
            'nombre'=>'Don Quijote de la Mancha',
            'editorial'=>'Destino',
            'autor'=>1,
            'portada'=>"portada1.jpg",
        ]);
        
        $this->insert("libros", [
            'nombre'=>'Un mundo feliz',
            'editorial'=>'DeBols!llo',
            'autor'=>2,
            'portada'=>"portada2.jpg",
        ]);
        
        $this->insert("libros", [
            'nombre'=>'Cien años de soledad',
            'editorial'=>'DeBols!llo',
            'autor'=>3,
            'portada'=>"portada3.jpg",
        ]);
        
        $this->insert("libros", [
            'nombre'=>'1984',
            'editorial'=>'DeBols!llo',
            'autor'=>4,
            'portada'=>"portada4.jpg",
        ]);
        
        $this->insert("libros", [
            'nombre'=>'Los Pilares de la Tierra',
            'editorial'=>'Plaza&Janes Editores',
            'autor'=>5,
            'portada'=>"portada5.jpg",
        ]);
        
        $this->insert("libros", [
            'nombre'=>'Orgullo y Prejuicios',
            'editorial'=>'Alianza Editorial',
            'autor'=>6,
            'portada'=>"portada6.jpg",
        ]);
        
        $this->insert("libros", [
            'nombre'=>'El Retrato de Dorian Gray',
            'editorial'=>'Literatura Random House',
            'autor'=>7,
            'portada'=>"portada7.jpg",
        ]);
        
        $this->insert("libros", [
            'nombre'=>'El Señor de los Anillos: La Comunidad del Anillo',
            'editorial'=>'Minotauro',
            'autor'=>8,
            'portada'=>"portada8.jpg",
        ]);
        
        $this->insert("libros", [
            'nombre'=>'El Señor de los Anillos: Las Dos Torres',
            'editorial'=>'Minotauro',
            'autor'=>8,
            'portada'=>"portada9.jpg",
        ]);
        
        $this->insert("libros", [
            'nombre'=>'Hamlet',
            'editorial'=>'Cátedra',
            'autor'=>9,
            'portada'=>"portada10.jpg",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('libros');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
    
    }

    public function down()
    {
        echo "m190402_111321_libros cannot be reverted.\n";

        return false;
    }
    */
}
